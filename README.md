# Vector Based Demo

Suporte a um vídeo sobre:

* a criação de repositórios no Bitbucket;
* ligar um projeto local a um repositório do bitbucket;
* correr e executar localmente testes de mutação; 
* e como efetuar refactoring de código, suportado por testes, mostrando algumas das técnicas utilizadas.

Haviam certamente outras formas de fazer isto e algumas até melhores, mas acho que esta solução de compromisso permite entregar a mensagem mais facilmente a todos os estudantes.

O vídeo acabou por ficar um pouco longo, por isso estejam à vontade para usar o VLC para o reproduzirem e aumentarem a taxa de reprodução para 2x ou até superior.

## Maven goals
### Run the unit tests
```
mvn clean test
```

### Generate javadoc for the source code
```
mvn javadoc:javadoc
```

### Generate javadoc for the test code
```
mvn javadoc:test-javadoc
```

### Generate Jacoco source code coverage report
```
mvn test jacoco:report
```

### Check if thresholds limits are achieved
```
mvn test jacoco:check
```

### Generates a PIT Mutation coverage report to target/pit-reports/YYYYMMDDHHMI
```
mvn org.pitest:pitest-maven:mutationCoverage
```

### Generates a quicker PIT Mutation coverage report to target/pit-reports/YYYYMMDDHHMI
```
mvn org.pitest:pitest-maven:mutationCoverage -DwithHistory
```

### Complete example

``` 
mvn test jacoco:report org.pitest:pitest-maven:mutationCoverage -DhistoryInputFile=target/fasterPitMutationTesting-history.txt -DhistoryOutputFile=target/fasterPitMutationTesting-history.txt -Dsonar.pitest.mode=reuseReport -Dthreads=4 -DtimestampedReports=false
```
## Jacoco dependencies
* https://github.com/pitest/pitest-junit5-plugin
  - https://mvnrepository.com/artifact/org.pitest/pitest-junit5-plugin
    - required to work with JUnit5
